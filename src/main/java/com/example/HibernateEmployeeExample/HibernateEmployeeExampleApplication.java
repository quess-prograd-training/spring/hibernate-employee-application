package com.example.HibernateEmployeeExample;

import com.example.HibernateEmployeeExample.components.Employee;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.util.List;
import java.util.Scanner;

@SpringBootApplication
public class HibernateEmployeeExampleApplication {

	public static void main(String[] args) {
		String ConfigurationFile ="hibernate.cfg.xml";
		ClassLoader classLoaderObj = HibernateEmployeeExampleApplication.class.getClassLoader();
		File f = new File(classLoaderObj.getResource(ConfigurationFile).getFile());
		SessionFactory sessionFactoryObject = new Configuration().configure(f).buildSessionFactory();
		Session sessionObject = sessionFactoryObject.openSession();
		Scanner scannerObj = new Scanner(System.in);
		System.out.println("Choose your choice given below!!");
		System.out.println("1.Create Employee");
		System.out.println("2.Display Employee");
		System.out.println("3.Update Employee");
		System.out.println("4.Delete Employee");
		System.out.println("5.List Employee");
		System.out.println("6.Exit");
		int choice = scannerObj.nextInt();
		switch (choice){
			case 1:
				saveRecord(sessionObject);
				break;
			case 2:
				display(sessionObject);
				break;
			case 3:
				update(sessionObject);
				break;
			case 4:
				delete(sessionObject);
				break;
			case 5:
				listEmployee(sessionObject);
				break;
			case 6:
				System.out.println("You are going to exit..");
				return;
			default:
				System.out.println("Invalid choice. Please enter a valid option..");
		}
		sessionObject.close();
	}
	private static void saveRecord(Session sessionObject){
		//Employee employeeObject = new Employee();

		Scanner scannerObj = new Scanner(System.in);
		System.out.println("Enter employee ID: ");
		Employee employeeObject = new  Employee();
		employeeObject.setId(scannerObj.nextInt());
		//int id = scannerObj.nextInt();

		/*if(employeeObject != null){
			System.out.println("Employee ID already exists.Please enter a different ID..");
			return;
		}*/
		System.out.println("Enter Employee name: ");
		scannerObj.nextLine();

		//String EmpName=scannerObj.nextLine();
		employeeObject.setEmpName(scannerObj.next());
		System.out.println("Enter employee salary: ");
		//int salary = scannerObj.nextInt();
		employeeObject.setSalary(scannerObj.nextInt());
		System.out.println("Enter employee department: ");
		scannerObj.nextLine();
		employeeObject.setDepartment(scannerObj.nextLine());
		//String department = scannerObj.nextLine();
		//employeeObject=new Employee(id,EmpName,salary,department);
		sessionObject.beginTransaction();
		sessionObject.save(employeeObject);
		sessionObject.getTransaction().commit();
		System.out.println("Employee added Successfully");

	}
	private static void display(Session sessionObject){
		Scanner scannerObj = new Scanner(System.in);
		System.out.println("Enter employee ID: ");
		int id=scannerObj.nextInt();
		Employee employeeObject = (Employee) sessionObject.get(Employee.class,id);
		if(employeeObject == null){
			System.out.println("Employee ID does not exist.");
			return;
		}
		System.out.println("Employee ID: " + employeeObject.getId());
		System.out.println("Employee name: " + employeeObject.getEmpName());;
		System.out.println("Employee salary: " + employeeObject.getSalary());;
		System.out.println("Employee department: "+ employeeObject.getDepartment());;
		sessionObject.beginTransaction();
		//sessionObject.save(employee1Object);
		sessionObject.getTransaction().commit();
	}
	private static void update(Session sessionObject){
		Scanner scannerObj = new Scanner(System.in);
		System.out.println("Enter employee ID: ");
		int id=scannerObj.nextInt();
		Employee employeeObject = (Employee) sessionObject.get(Employee.class,id);
		if(employeeObject == null){
			System.out.println("Employee ID does not exist.");
			return;
		}
		System.out.println("Enter new Employee name: ");
		scannerObj.nextLine();
		String EmpName=scannerObj.nextLine();
		System.out.println("Enter new employee salary: ");
		int salary = scannerObj.nextInt();
		System.out.println("Enter new employee department: ");
		String department = scannerObj.nextLine();
		scannerObj.nextLine();
		employeeObject.setEmpName(EmpName);
		employeeObject.setSalary(salary);
		employeeObject.setDepartment(department);
		employeeObject=new Employee(id,EmpName,salary,department);
		sessionObject.beginTransaction();
		sessionObject.update(employeeObject);
		sessionObject.getTransaction().commit();
		System.out.println("Employee updated Successfully..");
	}
	private static  void delete(Session sessionObject){
		Scanner scannerObj = new Scanner(System.in);
		System.out.println("Enter Employee ID: ");
		int id =scannerObj.nextInt();
		Employee employee1Object = (Employee) sessionObject.get(Employee.class,id);
		if(employee1Object==null){
			System.out.println("Employee ID does not exist..");
			return;
		}
		sessionObject.beginTransaction();
		sessionObject.delete(employee1Object);
		sessionObject.getTransaction().commit();
		System.out.println("Employee deleted Successfully..");
	}
	private static void listEmployee(Session sessionObject){
		Query queryObj = sessionObject.createQuery("FROM Employee1");
		List<Employee> listObj = queryObj.list();
		for (Employee iterator : listObj) {
			System.out.println(iterator.getId() + " " + iterator.getEmpName() + " " + iterator.getSalary() + " " + iterator.getDepartment());
		}
		System.out.println("The available data !!!");
		sessionObject.beginTransaction();
		sessionObject.getTransaction().commit();
	}

}
