package com.example.HibernateEmployeeExample.components;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Employee")
public class Employee {
    @Id
    @Column(name="id")
    private int id;
    @Column(name="EmpName")
    private String EmpName;
    private String department;
    private int salary;

    public Employee() {
    }

    public Employee(int id, String empName, String department, int salary) {
        this.id = id;
        EmpName = empName;
        this.department = department;
        this.salary = salary;
    }

    public Employee(int id, String empName, int salary, String department) {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmpName() {
        return EmpName;
    }

    public void setEmpName(String empName) {
        EmpName = empName;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
